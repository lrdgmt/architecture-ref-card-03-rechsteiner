# Architecture Ref. Card 03

## Projektbeschreibung
Die Dockerutility Ref. Card 03 kann man einen NGNX, Webserver mit einer Simplen HTML Seite in Betrieb nehmen.

## Voraussetzungen
Betriebssystem: Das Ref-Card-03-Projekt kann auf Linux (WSL) ausgeführt werden.
Docker: Sie müssen zwingend Docker installiert haben und die nötigen Berechtigungen besitzen.

## Installation und Verwendung
1. Klonen Sie das Repository auf Ihren lokalen Computer und navigieren Sie in das Projektverzeichnis:
    ``` bash
    git clone https://gitlab.com/lrdgmt/architecture-ref-card-03-rechsteiner
    cd architecture-ref-card-03-rechsteiner
    ```

2. Docker-Image erstellen:
    ``` bash
    docker build -t architecture-ref-card-03-rechsteiner .
    ```

3. Docker-Container starten:
    ``` bash
    docker run -d -p 80:80 architecture-ref-card-03-rechsteiner
    ```

4. Öffnen Sie Ihren Webbrowser und greifen Sie auf die Anwendung unter der folgenden URL zu: http://localhost

5. Sie sehen die bereitgestellte HTML-Seite, die im Container unter /usr/share/nginx/html/index.html liegt.